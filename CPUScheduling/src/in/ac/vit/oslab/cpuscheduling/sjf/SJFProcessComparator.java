package in.ac.vit.oslab.cpuscheduling.sjf;

import in.ac.vit.oslab.cpuscheduling.MyProcess;

import java.util.Comparator;

public class SJFProcessComparator implements Comparator<MyProcess> {

	@Override
	public int compare(MyProcess o1, MyProcess o2) {
		int result = ((Integer) o1.getBurstTime()).compareTo(o2.getBurstTime());
		return result != 0 ? result : ((Integer) o1.getArrivalTime())
				.compareTo(o2.getArrivalTime());
	}

}

package in.ac.vit.oslab.cpuscheduling.srtf;

import in.ac.vit.oslab.cpuscheduling.MyProcess;
import in.ac.vit.oslab.cpuscheduling.sjf.SJFProcessComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

public class SRTF {

	private ArrayList<MyProcess> processes;
	private int totalTime;
	private int timeNow = 1;
	private PriorityQueue<MyProcess> queue;

	public SRTF(ArrayList<MyProcess> process) {
		this.processes = process;
		this.queue = new PriorityQueue<>(this.processes.size(),
				new SJFProcessComparator());
	}

	public ArrayList<MyProcess> getProcesses() {
		return this.processes;
	}

	private int getStartTime() {
		Collections.sort(this.processes);
		return this.processes.get(0).getArrivalTime();
	}

	public int getTimeNow() {
		return this.timeNow;
	}

	public int getTotalTime() {
		return this.totalTime;
	}

	public ArrayList<MyProcess> run() {
		ArrayList<MyProcess> finishedProcesses = new ArrayList<MyProcess>(
				this.processes.size());
		this.timeNow = this.getStartTime();
		if (this.timeNow == 0) {
			this.timeNow = 1;
		}
		int runningProcesses = this.processes.size();
		int processesQueued = 0;
		while (runningProcesses != 0) {
			while ((processesQueued < this.processes.size())
					&& (this.processes.get(processesQueued).getArrivalTime() <= this.timeNow)) {
				this.queue.add(this.processes.get(processesQueued));
				processesQueued++;
			}
			if (this.queue.isEmpty()) {
				this.timeNow++;
			} else {
				MyProcess currentProcess = this.queue.poll();
				this.timeNow = currentProcess.run(this.timeNow, 1);
				if (currentProcess.getRemainingTime() == 0) {
					runningProcesses--;
					finishedProcesses.add(currentProcess);
				} else {

				}
			}
		}
		this.totalTime = this.timeNow - 1;
		return finishedProcesses;
	}

}
